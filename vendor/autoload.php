<?php

final class Autoloader
{
    protected $map = [];
    public function addNamespace($prefix, $dir)
    {
        $this->map[$prefix] = $dir;
    }

    public function register()
    {
        spl_autoload_register(array($this, 'loadClass'));
    }

    public function loadClass($class)
    {
        $pieces = explode('\\', $class);
        $prefix = array_shift($pieces);

        if (isset($this->map[$prefix])) {
            $path = __DIR__ . DIRECTORY_SEPARATOR . $this->map[$prefix] . DIRECTORY_SEPARATOR . implode(DIRECTORY_SEPARATOR, $pieces) . '.php';

            if (file_exists(realpath($path))) {
                require_once realpath($path);
            }
        }

    }
}
$autoloader = new Autoloader();
$autoloader->addNamespace('App', '../src');
$autoloader->register();